package com.wolfking.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;
import org.springframework.context.ApplicationContext;
import org.springframework.web.bind.annotation.RestController;
/**
 * 新配置中心的服务
 * @author wolfking@赵伟伟
 * @mail zww199009@163.com
 * @创作日期 2017年4月16日 下午9:59:19
 * @copyright wolfking
 */
@RestController
@SpringBootApplication
@EnableConfigServer
@EnableAutoConfiguration
public class ConfigServer {

	public static void main(String[] args) {
		SpringApplication application = new SpringApplication(ConfigServer.class);
		ApplicationContext applicationContext = application.run(args);
		System.out.println(applicationContext.getClass());
	}
}
