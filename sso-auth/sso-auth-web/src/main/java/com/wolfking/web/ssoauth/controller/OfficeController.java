package com.wolfking.web.ssoauth.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.wolfking.back.core.annotation.ssoauth.Sign;
import com.wolfking.back.core.bean.Area;
import com.wolfking.back.core.bean.Dict;
import com.wolfking.back.core.bean.Office;
import com.wolfking.back.core.bean.User;
import com.wolfking.back.core.config.SsoConfig;
import com.wolfking.back.core.feignclient.AreaFeignClient;
import com.wolfking.back.core.feignclient.DictFeignClient;
import com.wolfking.back.core.feignclient.OfficeFeignClient;
import com.wolfking.back.core.util.DataEntityUtil;

/**
 * 机构的controller
 * <P>
 * 
 * @author wolfking@赵伟伟
 * @mail zww199009@163.com
 * @创作日期 2017年5月3日下午5:12:06
 * @版权 归wolfking所有
 */
@Controller
@RequestMapping("/office")
public class OfficeController {

	@Autowired
	private AreaFeignClient areaFeignClient;

	@Autowired
	private DictFeignClient dictFeignClient;

	@Autowired
	private OfficeFeignClient officeFeignClient;

	@Autowired
	private SsoConfig ssoConfig;

	/**
	 * 机构的列表页面
	 * 
	 * @param model
	 * @return
	 */
	@Sign
	@RequestMapping(value = "/list", method = { RequestMethod.GET, RequestMethod.POST })
	public String list(Model model, Office office) {
		List<Office> list = Lists.newArrayList();
		List<Office> sourcelist;
		if (StringUtils.isBlank(office.getParentId()) && StringUtils.isBlank(office.getId())
				|| "0".equals(office.getParentId()))
			sourcelist = officeFeignClient.getAllOffice().getBody();
		else {
			sourcelist = officeFeignClient.getAllChildOffice(office.getId()).getBody();
			for (Office o : sourcelist)
				if (o.getId().equals(office.getId()))
					o.setParentId("0");
		}
		sortList(list, sourcelist, "0", true);
		model.addAttribute("list", list);
		List<Area> areas = areaFeignClient.getAllArea().getBody();
		Map<String, String> areaMap = new HashMap<>();
		List<Dict> dicts = dictFeignClient.getByType("sys_office_type").getBody();
		Map<String, String> dictMap = new HashMap<>();
		for (Area area : areas)
			areaMap.put(area.getId(), area.getName());
		for (Dict dict : dicts)
			dictMap.put(dict.getValue(), dict.getLabel());
		model.addAttribute("areaMap", areaMap);
		model.addAttribute("dictMap", dictMap);
		return "office/list";
	}

	/**
	 * 机构的编辑页面
	 * 
	 * @param model
	 * @param dict
	 * @return
	 */
	@Sign
	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public String edit(Model model, Office office) {
		Area area = new Area();
		Office parent = new Office();
		if (StringUtils.isNotBlank(office.getId()))
			office = officeFeignClient.getOffice(office.getId()).getBody();
		model.addAttribute("office", office);
		if (StringUtils.isNotEmpty(office.getAreaId()))
			area = areaFeignClient.getArea(office.getAreaId()).getBody();
		model.addAttribute("area", area);
		List<Dict> types = dictFeignClient.getByType("sys_office_type").getBody();
		model.addAttribute("types", types);
		List<Dict> grades = dictFeignClient.getByType("sys_office_grade").getBody();
		model.addAttribute("grades", grades);
		if (!"0".equals(office.getParentId()) && StringUtils.isNotEmpty(office.getParentId()))
			parent = officeFeignClient.getOffice(office.getParentId()).getBody();
		model.addAttribute("parent", parent);
		return "office/edit";
	}

	/**
	 * 机构的保存或者修改
	 * 
	 * @param model
	 * @param dict
	 * @return
	 */
	@Sign
	@RequestMapping(value = "/edit", method = RequestMethod.POST)
	public String edits(Office office, User user, RedirectAttributes redirectAttributes) {
		DataEntityUtil.assemblyDataEntity(office, user);
		if (StringUtils.isNotBlank(office.getId())) {
			officeFeignClient.updateOffice(office);
			redirectAttributes.addFlashAttribute("message", "修改机构成功");
		} else {
			officeFeignClient.addOffice(office);
			redirectAttributes.addFlashAttribute("message", "添加机构成功");
		}
		return "redirect:" + ssoConfig.getSelfRootUrl() + "/office/list";
	}

	/**
	 * 删除区域
	 * 
	 * @param id
	 * @return
	 */
	@Sign
	@RequestMapping(value = "/delete", method = RequestMethod.GET)
	public String delete(@RequestParam String id) {
		if (StringUtils.isNotBlank(id))
			officeFeignClient.deleteOffice(id);
		return "redirect:" + ssoConfig.getSelfRootUrl() + "/office/list";
	}

	@Sign
	@ResponseBody
	@RequestMapping(value = "/allTreeData", method = RequestMethod.GET)
	public List<Map<String, Object>> allTreeData() {
		List<Map<String, Object>> mapList = Lists.newArrayList();
		List<Office> list = officeFeignClient.getAllOffice().getBody();
		for (int i = 0; i < list.size(); i++) {
			Office e = list.get(i);
			Map<String, Object> map = Maps.newHashMap();
			map.put("id", e.getId());
			map.put("pId", e.getParentId());
			map.put("pIds", e.getParentIds());
			map.put("name", e.getName());
			map.put("type", e.getType());
			mapList.add(map);
		}
		return mapList;
	}

	@ResponseBody
	@RequestMapping(value = "treeData")
	public List<Map<String, Object>> treeData(@RequestParam(required = false) String extId,
			@RequestParam(required = false) String type, @RequestParam(required = false) Long grade,
			@RequestParam(required = false) Boolean isAll, HttpServletResponse response) {
		List<Map<String, Object>> mapList = Lists.newArrayList();
		List<Office> list = officeFeignClient.getAllOffice().getBody();
		for (int i = 0; i < list.size(); i++) {
			Office e = list.get(i);
			if (!e.getId().equals(extId) && "1".equals(e.getUseable())
					&& (StringUtils.isEmpty(type) || e.getType().equals(type))) {
				Map<String, Object> map = Maps.newHashMap();
				map.put("id", e.getId());
				map.put("pId", e.getParentId());
				map.put("pIds", e.getParentIds());
				map.put("name", e.getName());
				if (type != null && "3".equals(type)) {
					map.put("isParent", true);
				}
				mapList.add(map);
			}
		}
		return mapList;
	}

	@Sign
	@RequestMapping(value = "/index", method = RequestMethod.GET)
	public String index() {
		return "office/index";
	}

	private void sortList(List<Office> list, List<Office> sourcelist, String parentId, boolean cascade) {
		for (int i = 0; i < sourcelist.size(); i++) {
			Office e = sourcelist.get(i);
			if (parentId.equals(e.getParentId())) {
				list.add(e);
				if (cascade) {
					// 判断是否还有子节点, 有则继续获取子节点
					for (int j = 0; j < sourcelist.size(); j++) {
						Office child = sourcelist.get(j);
						if (e.getId().equals(child.getParentId())) {
							sortList(list, sourcelist, e.getId(), true);
							break;
						}
					}
				}
			}
		}
	}

}
