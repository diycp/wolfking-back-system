/**
 * 
 */
package com.wolfking.service.ssoauth.service;

import org.springframework.stereotype.Service;

import com.wolfking.back.core.bean.Menu;
import com.wolfking.back.core.mybatis.BaseService;
import com.wolfking.service.ssoauth.mapper.MenuMapper;

/**
 * 菜单的service
 * <P>
 * 
 * @author wolfking@赵伟伟
 * @mail zww199009@163.com
 * @创作日期 2017年4月27日下午1:59:38
 * @版权 归wolfking所有
 */
@Service
public class MenuService extends BaseService<MenuMapper, Menu> {

//	@Override
//	public Menu getById(Object id) {
//		Menu menu = super.getById(id);
//		if (StringUtils.isNotBlank(menu.getParentId())) {
//			Menu parent = super.getById(menu.getParentId());
//			if (parent != null)
//				menu.setParentName(parent.getName());
//		}
//		return menu;
//	}
//
//	/*
//	 * （非 Javadoc）
//	 * 
//	 * @see com.wolfking.back.core.mybatis.BaseService#update(java.lang.Object)
//	 */
//	@Transactional
//	public boolean update(Menu menu) {
//		mapper.update(menu);
//		String parentIds = menu.getParentIds() + menu.getId();
//		mapper.updateParentIdsByParentId(menu.getId(), parentIds);
//		return true;
//	}
//
//	@Transactional
//	public boolean deleteById(Object id) {
//		super.deleteById(id);
//		Menu menu = new Menu();
//		if (id instanceof String)
//			menu.setParentId(String.valueOf(id));
//		else
//			menu.setParentId(Menu.class.cast(id).getId());
//		mapper.deleteAccuracy(menu);
//		return true;
//	}
}
