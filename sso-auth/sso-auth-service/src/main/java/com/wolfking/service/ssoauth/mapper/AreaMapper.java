/**
 * 
 */
package com.wolfking.service.ssoauth.mapper;

import org.apache.ibatis.annotations.Mapper;

import com.wolfking.back.core.bean.Area;
import com.wolfking.back.core.mybatis.BaseMapper;

/**
 * 区域的mapper
 * <P>
 * @author   wolfking@赵伟伟
 * @mail     zww199009@163.com
 * @创作日期 2017年4月27日上午11:34:04
 * @版权     归wolfking所有
 */
@Mapper
public interface AreaMapper extends BaseMapper<Area> {

}
