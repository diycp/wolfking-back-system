package com.wolfking.service.ssoauth.resource;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;

import com.wolfking.back.core.bean.AccessLog;
import com.wolfking.back.core.bean.PageInfo;
import com.wolfking.back.core.util.ResponseUtil;
import com.wolfking.service.ssoauth.service.AccessLogService;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

@Component
@Path("/accessLog")
@Produces(MediaType.APPLICATION_JSON)
@Api(value = "/accessLog", description = "访问日志的服务", produces = MediaType.APPLICATION_JSON)
public class AccessLogResource {

	@Autowired
	private AccessLogService accessLogService;

	@POST
	@ApiOperation(value = "访问日志添加接口", httpMethod = "POST", notes = "访问日志添加接口", response = Boolean.class)
	public Response addAccessLog(@RequestBody AccessLog accessLog) {
		return ResponseUtil.okResponse(accessLogService.add(accessLog));
	}

	@PUT
	@ApiOperation(value = "访问日志修改接口", httpMethod = "PUT", notes = "访问日志修改接口", response = Boolean.class)
	public Response updateAccessLog(@RequestBody AccessLog accessLog) {
		return ResponseUtil.okResponse(accessLogService.update(accessLog));
	}

	@GET
	@Path("/{id}")
	@ApiOperation(value = "根据ID查询访问日志接口", httpMethod = "GET", notes = "根据ID查询访问日志接口", response = AccessLog.class)
	public Response getAccessLog(@PathParam("id") @ApiParam(required = true, name = "id", value = "用户ID") String id) {
		AccessLog accessLog = new AccessLog();
		accessLog.setId(id);
		accessLog = accessLogService.getById(accessLog);
		return ResponseUtil.okResponse(accessLog);
	}

	@DELETE
	@Path("/{id}")
	@ApiOperation(value = "根据ID删除访问日志接口", httpMethod = "DELETE", notes = "根据ID删除访问日志接口", response = Boolean.class)
	public Response deleteAccessLog(@PathParam("id") @ApiParam(required = true, name = "id", value = "用户ID") String id) {
		AccessLog accessLog = new AccessLog();
		accessLog.setId(id);
		return ResponseUtil.okResponse(accessLogService.deleteById(accessLog));
	}

	@POST
	@Path("/page")
	@ApiOperation(value = "分页查询访问日志信息", httpMethod = "POST", notes = "分页查询访问日志信息", response = AccessLog.class, responseContainer = "PageInfo")
	public Response pageAccessLog(@RequestBody AccessLog accessLog,
			@HeaderParam("pageNum") @ApiParam(name = "pageNum", value = "页码", defaultValue = "1") int pageNum,
			@HeaderParam("pageSize") @ApiParam(name = "pageSize", value = "每页大小", defaultValue = "10") int pageSize) {
		PageInfo<AccessLog> pageInfo = accessLogService.pageVague(accessLog, pageNum, pageSize);
		return ResponseUtil.okResponse(pageInfo);
	}

}
