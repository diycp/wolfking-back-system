package com.wolfking.service.ssoauth.service;

import org.springframework.stereotype.Service;

import com.wolfking.back.core.bean.Area;
import com.wolfking.back.core.mybatis.BaseService;
import com.wolfking.service.ssoauth.mapper.AreaMapper;

/**
 * 区域的service
 * <P>
 * 
 * @author wolfking@赵伟伟
 * @mail zww199009@163.com
 * @创作日期 2017年4月27日上午11:35:28
 * @版权 归wolfking所有
 */
@Service
public class AreaService extends BaseService<AreaMapper, Area> {
	
}
