package com.wolfking.service.ssoauth.config;

import javax.ws.rs.ApplicationPath;

import com.wolfking.back.core.invokelink.LinkRequestFilter;
import com.wolfking.back.core.invokelink.LinkResponseFilter;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.context.annotation.Configuration;

import com.wolfking.service.ssoauth.resource.AccessLogResource;
import com.wolfking.service.ssoauth.resource.AreaResource;
import com.wolfking.service.ssoauth.resource.DictResource;
import com.wolfking.service.ssoauth.resource.MenuResource;
import com.wolfking.service.ssoauth.resource.OfficeResource;
import com.wolfking.service.ssoauth.resource.RoleResource;
import com.wolfking.service.ssoauth.resource.SsoResource;
import com.wolfking.service.ssoauth.resource.UserResource;


@ApplicationPath("/wolfking")
@Configuration("wolfkingJerseyConfig")
public class JerseyConfig extends ResourceConfig {
	public JerseyConfig() {
		// swagger相关Provider
		packages("com.wordnik.swagger.jersey.listing");
		register(UserResource.class);
		register(SsoResource.class);
		register(DictResource.class);
		register(AreaResource.class);
		register(OfficeResource.class);
		register(MenuResource.class);
		register(AccessLogResource.class);
		register(RoleResource.class);
		register(LinkRequestFilter.class);
		register(LinkResponseFilter.class);
	}
}
