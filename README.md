spring-cloud的后台系统的服务和启动介绍
各个模块的介绍：
(1)back-core  一些通用的公共类
(2)service-discovery 服务注册与服务发现 -- 必须启动
(3)config-server     springcloud的配置中心
(4)service-gateway   服务网关，动态路由到具体服务
(5)sso-auth  登录和权限的 前台页面和后台服务
	a) sso-auth-service     登录和权限的后台服务
	b) sso-auth-controller  登录和权限的前台页面

最核心服务的运行，启动顺序
(1)service-discovery   必须第一个启动
(2)sso-auth-service    启动登录和权限的后台服务
(3)sso-auth-controller 启动登录和权限的前台页面

各个模块都要运行，必须使用网关服务
(1)service-discovery   必须第一个启动
(2)service-gateway   服务网关，动态路由到具体服务
(3)sso-auth-service    启动登录和权限的后台服务
(4)sso-auth-controller 启动登录和权限的前台页面
(5)其他服务启动
通过网关的访问模式
http://127.0.0.1/sso-auth/login