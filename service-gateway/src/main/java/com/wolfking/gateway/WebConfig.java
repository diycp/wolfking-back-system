package com.wolfking.gateway;

import org.springframework.boot.context.embedded.undertow.UndertowDeploymentInfoCustomizer;
import org.springframework.boot.context.embedded.undertow.UndertowEmbeddedServletContainerFactory;
import org.springframework.stereotype.Component;

import io.undertow.servlet.api.DeploymentInfo;

/**
 * 解决Zuul对某些异常返回200，且无返回内容的问题
 * <P>
 * 
 * @author wolfking@赵伟伟
 * @mail zww199009@163.com
 * @创作日期 2017年4月19日下午2:38:10
 * @版权 归wolfking所有
 */
@Component
public class WebConfig extends UndertowEmbeddedServletContainerFactory {

	public WebConfig() {
		addDeploymentInfoCustomizers(new UndertowDeploymentInfoCustomizer(){
			@Override
			public void customize(DeploymentInfo deploymentInfo) {
				deploymentInfo.setAllowNonStandardWrappers(true);
			}
		});
	}
}
