package com.wolfking.back.statics.freemarker;

import java.io.IOException;
import java.util.Map;
import java.util.Set;

import freemarker.core.Environment;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateDirectiveModel;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;

/**
 * freemarker的treeview的tag
 * <P>
 * 
 * @author wolfking@赵伟伟
 * @mail zww199009@163.com
 * @创作日期 2017年4月28日下午3:50:30
 * @版权 归wolfking所有
 */
@SuppressWarnings({ "rawtypes", "unchecked" })
public class TreeViewTag implements TemplateDirectiveModel {

	@Override
	public void execute(Environment env, Map params, TemplateModel[] loopVars, TemplateDirectiveBody body)
			throws TemplateException, IOException {
		if (params != null) {
			Set<Map.Entry> set = params.entrySet();
			for (Map.Entry entry : set)
				env.getCurrentNamespace().put("tree_" + String.valueOf(entry.getKey()), entry.getValue());
			body.render(env.getOut());
		}
	}
}
